# Angular Weather Forecast
An Angular weather app using OpenWeatherMap API.

# Third-party libraries
* ngx-charts
* ngx-toastr
* Bulma

# Setup
1. npm install
2. open the environment.ts and add your API key or set mock value to true if you don't have the API key.
3. ng serve

# Architectural choices
1. There is one smart/container/feature component **(weather-forecast)** 
and five dumb/pure/presentation components that are placed in the components folder. 
2. General styles have been defined in styles.scss. 
Spacing helpers are in spacing.scss and styles of each
component have been defined in its own styles file.
3. All resources are placed in the assets folder.
4. All pure functions do one task only. This creates a nice decoupling.

#  Features
* Mocking weather services by changing environment.ts (mock, apiKey)
* Coding standards and lint test
* Using semantic markups
* The ability to change the currentUnit (F of C) 
* Dumb and presentation component structure
* The ability to change theme gradient by modifying variables in variables.scss
* Using RXJS library for error handling and side effects
* Using async pipe
* Using pure functions
* Using JSONP
* Typescript: using types checking and interfaces
* Using a feature module! (weather-forecast) and only exporting what is needed for the appModule
* Using ngx-charts
* Responsive in most devices
* Comments
* Error handling
* Readability
* Extensibility
