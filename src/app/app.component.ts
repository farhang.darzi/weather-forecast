import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `<app-weather-forecast></app-weather-forecast>`,
})
export class AppComponent {

}
