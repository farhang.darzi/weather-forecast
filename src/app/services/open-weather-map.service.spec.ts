import { TestBed } from '@angular/core/testing';

import { OpenWeatherMapService } from './open-weather-map.service';


describe('OpenWeatherMapService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [OpenWeatherMapService,
      { provide: OpenWeatherMapService, useValue: {mock: false, apiKey: '6219cabf3383aae7e158b93b51fdd42f'} }]
  }));

  it('should be created', () => {
    const service: OpenWeatherMapService = TestBed.get(OpenWeatherMapService);
    expect(service).toBeTruthy();
  });
});
