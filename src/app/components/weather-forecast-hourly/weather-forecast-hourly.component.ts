import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-weather-forecast-hourly',
  templateUrl: './weather-forecast-hourly.component.html',
  styleUrls: ['./weather-forecast-hourly.component.scss']
})
export class WeatherForecastHourlyComponent {
  @Input()
  hourlyData;
}
