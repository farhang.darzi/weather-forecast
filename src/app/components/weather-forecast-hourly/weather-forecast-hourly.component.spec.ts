import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WeatherForecastHourlyComponent } from './weather-forecast-hourly.component';
import { SpinnerComponent } from '../spinner/spinner.component';

describe('WeatherForecastHourlyComponent', () => {
  let component: WeatherForecastHourlyComponent;
  let fixture: ComponentFixture<WeatherForecastHourlyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeatherForecastHourlyComponent, SpinnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherForecastHourlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
