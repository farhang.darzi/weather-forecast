import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CitiesComponent } from './cities.component';

describe('WeatherForecastDailyComponent', () => {
  let component: CitiesComponent;
  let fixture: ComponentFixture<CitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('On load selectedIndex must be zero', () => {
    expect(component.selectedIndex).toBe(0);
  });

  it('onCityClick, citySelected should emit data', (done) => {
    component.citySelected.subscribe(g => {
      expect(g).toEqual('Rotterdam');
      done();
    });
    component.onCityClick('Rotterdam', 1);
  });

  it('onCityClick, selectedIndex must be updated to proper value', () => {
    component.onCityClick('Rotterdam', 1);
    fixture.detectChanges();
    expect(component.selectedIndex).toEqual(1);
  });
});
