import { Component, EventEmitter, Input, Output, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.scss']
})
export class CitiesComponent {
  @Input() cities;
  @Output() citySelected =  new EventEmitter<any>();
  selectedIndex = 0;
  constructor() { }

  public onCityClick(selectedCityData: string, index: number): void {
    if ( this.selectedIndex  !== index) {
      this.citySelected.emit(selectedCityData);
      this.selectedIndex = index;
    }
  }
}
