import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-weather-forecast-hourly-table',
  templateUrl: './weather-forecast-hourly-table.component.html',
  styleUrls: ['./weather-forecast-hourly-table.component.scss']
})
export class WeatherForecastHourlyTableComponent {

  constructor() { }
  @Input() hourlyData: string;
}
