import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherForecastHourlyTableComponent } from './weather-forecast-hourly-table.component';
import { SpinnerComponent } from '../spinner/spinner.component';

describe('WeatherForecastHourlyTableComponent', () => {
  let component: WeatherForecastHourlyTableComponent;
  let fixture: ComponentFixture<WeatherForecastHourlyTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeatherForecastHourlyTableComponent, SpinnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherForecastHourlyTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
