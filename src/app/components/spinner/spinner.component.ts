import { Component } from '@angular/core';

@Component({
  selector: 'app-spinner',
  template: `<div class="center-flex-container">
    <img src="/assets/icons/spinner.svg" />
  </div>`,
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent {

}
