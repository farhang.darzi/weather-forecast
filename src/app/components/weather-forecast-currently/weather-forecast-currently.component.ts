import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-weather-forecast-currently',
  templateUrl: './weather-forecast-currently.component.html',
  styleUrls: ['./weather-forecast-currently.component.scss']
})
export class WeatherForecastCurrentlyComponent {
  @Input() currentWeatherData;
  @Input() units;
  @Output() unitWasSet =  new EventEmitter<any>();
  constructor() { }

  /**
   *  Emit weather unit to Onchange Output Event Emitter.
   */
  public setCurrentUnit(unit) {
    this.unitWasSet.emit(unit);
  }
}
