import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherForecastCurrentlyComponent } from './weather-forecast-currently.component';
import { SpinnerComponent } from '../spinner/spinner.component';

describe('WeatherForecastCurrentlyComponent', () => {
  let component: WeatherForecastCurrentlyComponent;
  let fixture: ComponentFixture<WeatherForecastCurrentlyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeatherForecastCurrentlyComponent, SpinnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherForecastCurrentlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit selected unit whenever user clicks on C | F', (done) => {
    component.unitWasSet.subscribe(g => {
      expect(g).toEqual('metric');
      done();
    });
    component.setCurrentUnit('metric');
  });

});
