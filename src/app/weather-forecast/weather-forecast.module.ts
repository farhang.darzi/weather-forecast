import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeatherForecastComponent } from './weather-forecast.component';
import { WeatherForecastHourlyComponent } from '../components/weather-forecast-hourly/weather-forecast-hourly.component';
import { CitiesComponent } from '../components/cities/cities.component';
import { WeatherForecastCurrentlyComponent } from '../components/weather-forecast-currently/weather-forecast-currently.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { WeatherForecastHourlyTableComponent } from '../components/weather-forecast-hourly-table/weather-forecast-hourly-table.component';
import { SpinnerComponent } from '../components/spinner/spinner.component';

@NgModule({
  imports: [
    CommonModule,
    NgxChartsModule,

  ],
  declarations: [
    WeatherForecastComponent,
    WeatherForecastHourlyComponent,
    WeatherForecastCurrentlyComponent,
    WeatherForecastHourlyTableComponent,
    CitiesComponent,
    SpinnerComponent
  ],
  exports: [
    WeatherForecastComponent
  ],
})
export class WeatherForecastModule {}

