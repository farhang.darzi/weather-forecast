import { Component, OnInit } from '@angular/core';
import { OpenWeatherMapService, OpenWeatherMapOptions } from '../services/open-weather-map.service';
import { ErrorHandlingService } from '../services/error-handling.service';
import * as shape from 'd3-shape';
import { catchError, share, tap } from 'rxjs/internal/operators';
import { Observable, of } from 'rxjs/index';

@Component({
  selector: 'app-weather-forecast',
  templateUrl: './weather-forecast.component.html',
  styleUrls: ['./weather-forecast.component.scss'],
  providers: [OpenWeatherMapService]
})
export class WeatherForecastComponent implements OnInit {
  cities: Array<string> = ['Amsterdam', 'Rotterdam', 'Groningen', 'Tilburg', 'Nijmegen'];
  apiOptions: OpenWeatherMapOptions = {units: 'metric', q: this.cities[0]};
  chartData: Array<object>;
  curve = shape.curveCardinal;
  colorScheme = {domain: ['#FFFE4550', '#94cadd50']};
  mainWeather$: Observable<any>;
  mainForecast$: Observable<any>;
  constructor(private openWeatherMapService: OpenWeatherMapService,
              private errorHandlingService: ErrorHandlingService,
              ) {}

  ngOnInit() {
    this.getWeather();
    this.getForecast();
  }

  public unitClicked(unit: string) {
    this.setCurrentUnit(unit);
    this.getWeather();
    this.getForecast();
  }

  /**
   * Calling getWeather() and getForecast() method when
   * user clicks on a city item to fetch
   * weather data in defined unit.
   */
  public getWeatherByCity(city: string): void {
    this.apiOptions.q = city;
    this.getWeather();
    this.getForecast();
  }

  /**
   * Get current Weather.
   */
  private getWeather() {
    this.mainWeather$ = this.openWeatherMapService.getWeather(this.apiOptions).pipe(
      catchError((error) => {
        this.errorHandlingService.DisplayErrors(error.message);
        return of();
      })
    );
  }

  /**
   * Get 5 days Weather.
   */
  private getForecast() {
    this.mainForecast$ = this.openWeatherMapService.getForecast(this.apiOptions).pipe(
      catchError((error) => {
        this.errorHandlingService.DisplayErrors(error.message);
        return of();
      }),
      share(),
      tap((data) => {
        this.setChartData(data.list);
        return data;
      })
    );
  }

  /**
   * Set unit property of apiOptions
   * @param unit which select by user.
   */
  private setCurrentUnit(unit: string) {
    if (unit !== this.apiOptions.units) {
      this.apiOptions.units = unit;
    }
  }

  /**
   * Set chart data.
   * @param data object of weather hourly data.
   * @return chart temperature and wind data in proper format.
   */
  private setChartData(data): void {
    const temp = this.manipulateChartData(data, 'main', 'temp');
    const wind = this.manipulateChartData(data, 'wind', 'speed');
    this.chartData = [{name: 'Temperature', series: temp}, {name: 'Wind Speed', series: wind}];
  }

  /**
   * filter and return next 3 days data in every 3 hours.
   */
  private manipulateChartData(data, firstArg: string, secondArg: string) {
  return data.filter((elem, i) => {
      if (i <= 7) {
        return data[i];
      }
    })
      .map((el, i) => {
        return {value: Math.round(el[firstArg][secondArg]), name: i};
      });
  }
}
